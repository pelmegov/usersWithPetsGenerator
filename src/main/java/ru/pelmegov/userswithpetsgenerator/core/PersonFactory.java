package ru.pelmegov.userswithpetsgenerator.core;

import ru.pelmegov.userswithpetsgenerator.animal.Animal;
import ru.pelmegov.userswithpetsgenerator.person.Person;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class PersonFactory {

    private static Collection<Person> persons = new ArrayList<Person>();

    private static List<String> names = new ArrayList<String>();

    public static Collection<Person> getPersons(int personCount, int animalCount) {
        try {
            loadFile("src/main/resources/name_rus.txt");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < personCount; i++) {
            Collection<Animal> animals = new ArrayList<Animal>();
            for (int j = 0; j < new Random().nextInt(animalCount + 1); j++) {
                animals.add(new Animal(new Random().nextInt(15), getName()));
            }
            persons.add(new Person(i, new Random().nextInt(80), getName(), animals));
        }
        return persons;
    }

    public static Collection<Person> getPersons(int personCount) {
        return getPersons(personCount, 0);
    }

    private static List loadFile(String file) throws FileNotFoundException {
        Scanner in = new Scanner(new File(file));
        while (in.hasNextLine()) {
            String nextLine = in.nextLine();
            names.add(nextLine.substring(0, 1).toUpperCase() + nextLine.substring(1));
        }
        return names;
    }

    private static String getName() {
        Random random = new Random();
        return names.get(random.nextInt(names.size()));
    }

}
