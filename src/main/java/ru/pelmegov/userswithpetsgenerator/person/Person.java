package ru.pelmegov.userswithpetsgenerator.person;

import ru.pelmegov.userswithpetsgenerator.animal.Animal;

import java.util.Collection;
import java.util.List;

public class Person {

    private int id = 0;
    private int age;
    private String name;
    private Collection<Animal> animals;

    public Person(int id, int age, String name, Collection<Animal> animals) {
        this.id = id;
        this.age = age;
        this.name = name;
        this.animals = animals;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Collection<Animal> getAnimals() {
        return animals;
    }

    public void setAnimals(List<Animal> animals) {
        this.animals = animals;
    }

    @Override
    public String toString() {
        String animal = "";
        if (animals.size() > 0)
            animal = ", animals=" + animals;

        return id + ") " + name + "\n" +
                "age=" + age + animal + "\n";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Person)) return false;
        Person person = (Person) o;
        if (getId() != person.getId()) return false;
        if (getAge() != person.getAge()) return false;
        if (!getName().equals(person.getName())) return false;
        return getAnimals().equals(person.getAnimals());
    }

    @Override
    public int hashCode() {
        int result = getId();
        result = 31 * result + getAge();
        result = 31 * result + getName().hashCode();
        result = 31 * result + getAnimals().hashCode();
        return result;
    }

}
