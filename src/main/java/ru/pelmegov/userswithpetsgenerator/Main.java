package ru.pelmegov.userswithpetsgenerator;

import ru.pelmegov.userswithpetsgenerator.core.PersonFactory;
import ru.pelmegov.userswithpetsgenerator.person.Person;

import java.util.Collection;

public class Main {
    public static void main(String[] args) {

        // PersonFactory.getPersons(Количество user, max количество animal каждого user)
        Collection<Person> persons = PersonFactory.getPersons(1000, 2);
        for (Person person : persons) {
            System.out.println(person);
        }
    }
}
